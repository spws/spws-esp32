#pragma once

#include "config.h"

#include "humidity_sensor.h"
#include "pump.h"
#include "mqtt.h"
#include "led.h"
#include "calibration/calibration_data.h"

class Plant
{
public:
  Plant(uint32_t id, const char *name, HumiditySensor *humiditySensor, Pump *pump, Led *led, int humidityLowThreshold);

  /**
   * @brief Initialize the object
   *
   * @param irrigationDuration_s How long the pump shall be activated.
   */
  void begin(const calibrationData_t *calibrationData);

  /**
   * @brief Start measuring the humidity of the plant.
   */
  void startMeasurement();

  /**
   * @brief Measure the humidity of the plant.
   *
   * @return True when measurement is ready, false otherwise.
   */
  bool measure();

  /**
   * @brief Check if the plant needs to be irrigated.
   *
   * @return True if it needs to be irrigated, false otherwise.
   */
  bool needsIrrigation();

  /**
   * @brief Start the pump.
   */
  void startIrrigation();

  /**
   * @brief This function irrigates the plant for the calibrated duration.
   *
   * @details This function stops the pump after the calibrated time.
   *          startIrrigation() has to be called first. This function should
   *          be called until it return false to avoid water damage.
   *
   * @return True als long as the pump is running, false when the pump is stopped.
   */
  bool irrigate();

  /**
   * @brief Get the minimum and the maximum measured humidity values
   *        and recalibrate the sensor if neccesarry.
   */
  void calibrateHumidityLimits();

  /**
   * @brief Get the maximum measured humidity values
   *        and recalibrate the sensor if neccesarry.
   */
  void calibrateUpperHumidityLimit();

  /**
   * @brief Get the minimum measured humidity values
   *        and recalibrate the sensor if neccesarry.
   */
  void calibrateLowerHumidityLimit();

  /**
   * @brief Compare humidity before and after irrigation to check
   *        if next irrigation needs to be longer.
   */
  void calibrateIrrigationDuration();

  /**
   * @brief Get the currenly calibrated values.
   *
   * @param[out] calibrationData Calibration values from this object.
   */
  void getCurrentCalibration(calibrationData_t *calibrationData);

  /**
   * @brief Get the unique ID of the plant.
   *
   * @return uint32_t ID of the plant
   */
  uint32_t getID();

  /**
   * @brief Make the plant specific led blink.
   *
   * @param interval_ms Led will be on for interval ms and off for interval ms.
   */
  void ledBlink(uint32_t interval_ms);

  /**
   * @brief Turn the plant specific led off.
   */
  void ledOff();

  /**
   * @brief Turn the plant specific led on.
   */
  void ledOn();

private:
  uint32_t id;
  HumiditySensor *humiditySensor;
  Pump *pump;
  Led *led;
  int8_t humidityLowThreshold;
  const int8_t targetHumidity = 70;
  const uint8_t targetTollerance = 1;
  unsigned int irrigationDuration_ms;
  const char *name;
  unsigned long irrigationStartedAt_ms;
};
