#pragma once

#include "Arduino.h"

class Led
{
public:
  Led(uint8_t pin);

  void begin();

  void blink(uint32_t interval_ms);
  void turnOn();
  void turnOff();
  void handle();
  void changeStatus();

private:
  uint8_t pin;
  bool blinking;
  uint32_t blinkInterval_ms;
  uint32_t blinkChangedAt_ms;
  uint8_t currentState;
};
