#include "config.h"

#include "Arduino.h"
#include "water_tank.h"

#pragma once

class Pump
{
private:
	uint8_t powerPin;
	WaterTank *water_tank;
public:
	Pump(uint8_t powerPin, WaterTank *water_tank);
	void pumpForSeconds(unsigned int duration);
	void activate(void);
	void deactivate(void);
};
