#include "Arduino.h"

#include "config.h"
#include "plant.h"
#include "led.h"
#include "button.h"
#include "plant_fsm.h"
#include "calibration/calibration_fsm.h"

#pragma once

typedef enum
{
  MAIN_FSM_IDLE,
  MAIN_FSM_RUNNING,
  MAIN_FSM_CALIBRATION,
  MAIN_FSM_ERROR
} MainFsmState_t;

class MainFsm
{
public:
  void begin(
    Plant *plants,
    CalibrationStorage *calibrationStorage,
    Led *ledCalibrationWater,
    Led *ledCalibrationDry,
    Button *buttonStart,
    Button *buttonSet
  );

  void run();

  MainFsmState_t getCurrentState();
private:
  bool initialized = false;

  bool isCalibrationAllowed = true;
  const uint32_t calibrationAllowedUntil_ms = 500;

  MainFsmState_t currentState;
  MainFsmState_t lastState;

  PlantFsm plantFsms[CONFIG_PLANT_COUNT];
  CalibrationFsm calibrationFsms[CONFIG_PLANT_COUNT];

  size_t currentCalibrationFsm;

  Plant *plants = nullptr;
  CalibrationStorage *calibrationStorage = nullptr;

  Led *ledCalibrationWater = nullptr;
  Led *ledCalibrationDry = nullptr;

  Button *buttonStart = nullptr;
  Button *buttonSet = nullptr;

  MainFsmState_t idleState();
  MainFsmState_t runningState();

  void calibrationStateEnter();
  MainFsmState_t calibrationState();
};
