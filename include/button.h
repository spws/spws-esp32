#include "Arduino.h"

#pragma once

class Button
{
public:
  Button(uint8_t pin, uint8_t pressedAt, uint32_t debounceTime_ms);

  void begin();
  bool isPressed();

  void handle();

private:
  uint8_t pin;
  uint8_t activeAt;

  uint32_t debounceTime_ms;
  uint32_t debounceTimeStartetAt_ms;
  int currentState;
};
