#include "Arduino.h"
#include "PubSubClient.h"

#include "config.h"

#pragma once

class Mqtt
{
private:
	static PubSubClient *mqtt_client;
	static const char *client_name;

	static bool connect();
public:
	static void init(const char *client_name, Client& network_client, const char *server_ip, uint16_t server_port);
	static bool publish(const char *topic, const char *payload);
	static bool publish_humidity(const char *plant_name, int humidity);
};
