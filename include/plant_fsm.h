#include "Arduino.h"

#include "plant.h"

#pragma once

typedef enum
{
  PLANT_FSM_IDLE,
  PLANT_FSM_MEASURING,
  PLANT_FSM_IRRIGATING,
  PLANT_FSM_WAITING,
  PLANT_FSM_CALIBRATING,
  PLANT_FSM_ERROR,
} PlantFsmState_t;

class PlantFsm
{
public:
  void run();
  void begin(Plant *plant);
  PlantFsmState_t getCurrentState();
private:
  PlantFsmState_t currentState;
  PlantFsmState_t lastState;
  Plant *plant;
  uint32_t waitingStartedAt_ms;
  const uint32_t waitingTime_ms = 1000 * 60 * 5; /* 5 Minutes */

  void measuringStateEnter();
  PlantFsmState_t measuringState();

  void irrigatingStateEnter();
  PlantFsmState_t irrigatingState();

  void waitingStateEntered();
  PlantFsmState_t waitingState();

  void calibratingStateEntered();
  PlantFsmState_t calibrationState();
};
