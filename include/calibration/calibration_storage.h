#include "Arduino.h"

#include "config.h"
#include "calibration/calibration_data.h"

#pragma once

class CalibrationStorage
{
public:
  void begin();
  bool getCalibrationData(size_t plantID, calibrationData_t *calibrationData);
  void writeCalibrationData(size_t plantID, calibrationData_t *calibrationData);

private:
  bool initialized = false;
};
