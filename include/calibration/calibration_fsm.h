#include "Arduino.h"

#include "plant.h"
#include "button.h"
#include "led.h"
#include "calibration/calibration_data.h"

#pragma once

typedef enum
{
  CALIBRATION_FSM_IDLE,
  CALIBRATION_FSM_START,
  CALIBRATION_FSM_START_DRY,
  CALIBRATION_FSM_MEASURE_DRY,
  CALIBRATION_FSM_START_WATER,
  CALIBRATION_FSM_MEASURE_WATER,
  CALIBRATION_FSM_FINISH,
  CALIBRATION_FSM_ERROR,
} CalibrationFsmState_t;

class CalibrationFsm
{
public:
  void run();
  void begin(
    Plant *plant,
    CalibrationStorage *calibrationStorage,
    Led *ledCalibrationWater,
    Led *ledCalibrationDry,
    Button *buttonStart,
    Button *buttonSet
  );
  CalibrationFsmState_t getCurrentState();
  void startCalibration();
private:
  CalibrationFsmState_t currentState;
  CalibrationFsmState_t lastState;
  Plant *plant;
  calibrationData_t calibrationData; /* TODO: Required? */
  CalibrationStorage *calibrationStorage;
  bool initialized = false;

  bool calibrationRequested = false;

  Led *ledCalibrationWater = nullptr;
  Led *ledCalibrationDry = nullptr;

  Button *buttonStart = nullptr;
  Button *buttonSet = nullptr;

  CalibrationFsmState_t idleState();

  void startStateEnter();
  CalibrationFsmState_t startState();

  void startDryStateEnter();
  CalibrationFsmState_t startDryState();

  void measureDryStateEnter();
  CalibrationFsmState_t measureDryState();

  void startWaterStateEnter();
  CalibrationFsmState_t startWaterState();

  void measureWaterStateEnter();
  CalibrationFsmState_t measureWaterState();

  CalibrationFsmState_t finishState();
};
