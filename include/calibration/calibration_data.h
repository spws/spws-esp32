#include "Arduino.h"

#include "config.h"

#pragma once

typedef struct __attribute__((__packed__))
{
  uint16_t minValue;
  uint16_t maxValue;
  uint32_t irrigationDuration_ms;
  uint16_t crc;
} calibrationData_t;

bool calibrationData_isValid(const calibrationData_t *calibrationData);
void calibrationData_updateCrc(calibrationData_t *calibrationData);
uint16_t calibrationData_calculateCrc(const calibrationData_t *calibrationData);

calibrationData_t calibrationData_getDefault(void);
