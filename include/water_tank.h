
#include "Arduino.h"

#pragma once

class WaterTank
{
private:
	uint8_t sensorPin;
	bool empty_when_sensor_closed;
public:
	WaterTank(uint8_t sensorPin, bool empty_when_sensor_closed);
	bool isEmpty();
};
