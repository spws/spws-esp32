#include "config.h"

#include "Arduino.h"

#include "calibration/calibration_data.h"
#include "pump.h"

#pragma once

class HumiditySensor
{
public:
  HumiditySensor(uint8_t sensorPin, uint8_t powerPin);

  /**
   * @brief Initialize the object.
   *
   * @param minValue Minimum value the sensor can output on the adc pin.
   * @param maxValue Maximum value the sensor can output on the adc pin.
   */
  void begin(const calibrationData_t *calibrationData);

  /**
   * @brief Measure the adc value and add it to the current measurement.
   *
   * @return True when measurement is finished.
   */
  bool measure();

  /**
   * @brief Initialize object for a new measurement.
   *
   */
  void startMeasurement();

  /**
   * @brief Get the Measured Humidity after measure returned true;
   *
   * @return Humidity between 0 and 100 in percent. < 0 on error.
   */
  int8_t getMeasuredHumidity();

  /**
   * @brief Set the power pin for the sensor to high.
   *
   */
  void powerOn(void);

  /**
   * @brief Set the power pin for the sensor to low.
   *
   */
  void powerOff(void);

  /**
   * @brief Read the adc value from the sensor data pin.
   *
   * @return 12 bit adc value.
   */
  uint16_t readDataPin(void);

  /**
   * @brief Get the minimum measured value from the samples. Only call it after
   *        the measurement was done. This function is usefull for calibration.
   *
   * @return Minimum value from the measured samples.
   */
  uint16_t getMinimumMeasuredValue();

  /**
   * @brief Get the maximum measured value from the samples. Only call it after
   *        the measurement was done. This function is usefull for calibration.
   *
   * @return Maximum value from the measured samples.
   */
  uint16_t getMaximumMeasuredValue();

  /**
   * @brief Get the minimum and the maximum measured humidity values
   *        and recalibrate the sensor if neccesarry.
   */
  void calibrateHumidityLimits();

  /**
   * @brief Get the maximum measured humidity values
   *        and recalibrate the sensor if neccesarry.
   */
  void calibrateUpperHumidityLimit();

  /**
   * @brief Get the minimum measured humidity values
   *        and recalibrate the sensor if neccesarry.
   */
  void calibrateLowerHumidityLimit();

  /**
   * @brief Get the calibrated maximum sensor adc value.
   *
   * @return uint16_t Maximum sensor value
   */
  uint16_t getMaxValue();

  /**
   * @brief Get the calibrated minimum sensor adc value.
   *
   * @return uint16_t Minimum sensor value
   */
  uint16_t getMinValue();

private:
  uint8_t sensorPin;
  uint8_t powerPin;

  constexpr static unsigned int MEASUREMENT_AMOUNT = 10;
  static_assert(HumiditySensor::MEASUREMENT_AMOUNT > 0, "measurement_amount cannot be 0 or less.");

  /**
   * The minimum and maximum values that can be measured depend on the power
   * supply and the used sensor. These values are needed to calibrate the sensor.
   */
  uint16_t maxValue = 4095;
  uint16_t minValue = 0;

  uint16_t samples[MEASUREMENT_AMOUNT];
  size_t currentSampleIndex;
  unsigned long lastMeasurementAt_ms;

  /**
   * @brief Calculate the humidity value from the samples array in percent.
   *
   * @return Humidity value in percent. (0 - 100)
   */
  uint8_t calculateHumidityInPercent();
};
