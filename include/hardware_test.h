#include "Arduino.h"

#include "water_tank.h"
#include "humidity_sensor.h"
#include "pump.h"

#pragma once

void runHardwareTest(
	WaterTank &water_tank,
	HumiditySensor humiditySensors[],
	Pump pumps[],
	unsigned int plant_count
);
