#include "Arduino.h"

#pragma once

/**
 *	crc_ccitt - recompute the CRC-CCITT for the data buffer.
  *	@param buffer: data pointer
  *	@param len: number of bytes in the buffer
  */
uint16_t crc_ccitt(const uint8_t *buffer, size_t len);
