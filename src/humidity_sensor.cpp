#include <math.h>
#include "humidity_sensor.h"

HumiditySensor::HumiditySensor(uint8_t sensorPin, uint8_t powerPin)
{
  this->sensorPin = sensorPin;
  this->powerPin = powerPin;
}

void HumiditySensor::begin(const calibrationData_t *calibrationData)
{
  assert(minValue < maxValue);

  this->minValue = calibrationData->minValue;
  this->maxValue = calibrationData->maxValue;

  pinMode(sensorPin, INPUT);
  pinMode(powerPin, OUTPUT);

  /* This should be the default state, but just to be sure... */
  powerOff();
}

uint8_t HumiditySensor::calculateHumidityInPercent()
{
  uint8_t dryness_percentage = 0;
  uint16_t norm_average = 0;
  uint16_t norm_max = 0;
  uint16_t average = 0;
  uint32_t sum = 0;
  size_t i = 0;

  /* Works for up to 65536 samples. After that, an overflow could occur. */
  for (i = 0; i < MEASUREMENT_AMOUNT; i++)
  {
    sum += samples[i];
  }

  average = (uint16_t) (sum / MEASUREMENT_AMOUNT);
  norm_max = maxValue - minValue;
  if (average < minValue)
  {
    norm_average = 0;
  }
  else
  {
    norm_average = average - minValue;
  }

  if (norm_average > norm_max)
  {
    norm_average = norm_max;
  }

  dryness_percentage = (uint8_t) (((uint32_t) norm_average * 100u) / norm_max);

  /* The sensor actually reads the dryness of the soil so we need to
   * calculate the humidity here.
   */
  return 100u - dryness_percentage;
}

void HumiditySensor::startMeasurement()
{
  currentSampleIndex = 0;

  /* Used to delay measurement to ensure sensor is powered up. */
  lastMeasurementAt_ms = millis();
  powerOn();
}

bool HumiditySensor::measure()
{
  /* Wait 500 ms between each measurement. This has to be long enough for the sensor to power up. */
  constexpr unsigned long waitTime_ms = 500;
  unsigned long currentTime_ms = millis();

  if (currentSampleIndex == MEASUREMENT_AMOUNT)
  {
    /* Measurement already finished. To start a new one, call startMeasurement(). */
    return true;
  }

  if (currentTime_ms - lastMeasurementAt_ms < waitTime_ms)
  {
    /* No measurement because not enough time passed. */
    return false;
  }

  /* Measurement */
  samples[currentSampleIndex] = readDataPin();
  currentSampleIndex++;
  lastMeasurementAt_ms = currentTime_ms;

  if (currentSampleIndex == MEASUREMENT_AMOUNT)
  {
    /* Measurement ready. */
    powerOff();
    return true;
  }

  return false;
}

int8_t HumiditySensor::getMeasuredHumidity()
{
  if (currentSampleIndex != MEASUREMENT_AMOUNT)
  {
    /* Measurement not ready. */
    return -1;
  }

  return calculateHumidityInPercent();
}

uint16_t HumiditySensor::getMinimumMeasuredValue()
{
  /* Only call this function if measurement is finished. */
  assert(currentSampleIndex == MEASUREMENT_AMOUNT);

  size_t i = 0;
  uint16_t minimum = samples[0];

  for (i = 1; i < MEASUREMENT_AMOUNT; i++)
  {
    if (samples[i] < minimum)
    {
      minimum = samples[i];
    }
  }

  return minimum;
}

uint16_t HumiditySensor::getMaximumMeasuredValue()
{
  /* Only call this function if measurement is finished. */
  assert(currentSampleIndex == MEASUREMENT_AMOUNT);

  size_t i = 0;
  uint16_t maximum = samples[0];

  for (i = 1; i < MEASUREMENT_AMOUNT; i++)
  {
    if (samples[i] > maximum)
    {
      maximum = samples[i];
    }
  }

  return maximum;
}

void HumiditySensor::powerOn(void)
{
  digitalWrite(this->powerPin, HIGH);
}

void HumiditySensor::powerOff(void)
{
  digitalWrite(this->powerPin, LOW);
}

uint16_t HumiditySensor::readDataPin(void)
{
  return analogRead(this->sensorPin);
}

void HumiditySensor::calibrateHumidityLimits()
{
  calibrateLowerHumidityLimit();
  calibrateUpperHumidityLimit();
}

void HumiditySensor::calibrateUpperHumidityLimit()
{
  uint16_t upperLimit = getMaximumMeasuredValue();
  this->maxValue = upperLimit;
}

void HumiditySensor::calibrateLowerHumidityLimit()
{
  uint16_t lowerLimit = getMinimumMeasuredValue();
  this->minValue = lowerLimit;
}

uint16_t HumiditySensor::getMinValue()
{
  return this->minValue;
}

uint16_t HumiditySensor::getMaxValue()
{
  return this->maxValue;
}
