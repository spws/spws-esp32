#include "Arduino.h"
#include "calibration/calibration_data.h"
#include "EEPROM.h"
#include "string.h" /* memcpy() */
#include "calibration/calibration_storage.h"

void CalibrationStorage::begin()
{
  EEPROM.begin(512);
  initialized = true;
}

bool CalibrationStorage::getCalibrationData(size_t plantID, calibrationData_t *calibrationData)
{
  assert(initialized);

  EEPROM.get(plantID * sizeof(*calibrationData), *calibrationData);
  return calibrationData_isValid(calibrationData);
}

void CalibrationStorage::writeCalibrationData(size_t plantID, calibrationData_t *calibrationData)
{
  assert(initialized);

  int address = sizeof(*calibrationData) * plantID;

  calibrationData_updateCrc(calibrationData);

  EEPROM.put(address, *calibrationData);
  EEPROM.commit();
}
