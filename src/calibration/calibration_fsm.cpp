#include "calibration/calibration_storage.h"
#include "calibration/calibration_fsm.h"

void CalibrationFsm::begin(
  Plant *plant,
  CalibrationStorage *calibrationStorage,
  Led *ledCalibrationWater,
  Led *ledCalibrationDry,
  Button *buttonStart,
  Button *buttonSet
)
{
  bool valid = false;

  this->plant = plant;
  this->calibrationStorage = calibrationStorage;

  valid = calibrationStorage->getCalibrationData(plant->getID(), &this->calibrationData);
  if (!valid)
  {
    calibrationData = {0};
    // assert(false);
  }

  this->ledCalibrationDry = ledCalibrationDry;
  this->ledCalibrationWater = ledCalibrationWater;

  this->buttonSet = buttonSet;
  this->buttonStart = buttonStart;

  initialized = true;
}

void CalibrationFsm::run()
{
  assert(initialized);

  CalibrationFsmState_t nextState = currentState;

  if (currentState != lastState)
  {
    printf("new calibration fsm state: %d\n", currentState);
  }

  switch (currentState)
  {
  case CALIBRATION_FSM_IDLE:
  {
    nextState = idleState();
    break;
  }
  case CALIBRATION_FSM_START:
  {
    if (lastState != currentState)
    {
      startStateEnter();
    }

    nextState = startState();
    break;
  }
  case CALIBRATION_FSM_START_DRY:
  {
    if (lastState != currentState)
    {
      startDryStateEnter();
    }

    nextState = startDryState();
    break;
  }
  case CALIBRATION_FSM_MEASURE_DRY:
  {
    if (lastState != currentState)
    {
      measureDryStateEnter();
    }

    nextState = measureDryState();
    break;
  }
  case CALIBRATION_FSM_START_WATER:
  {
    if (lastState != currentState)
    {
      startWaterStateEnter();
    }

    nextState = startWaterState();
    break;
  }
  case CALIBRATION_FSM_MEASURE_WATER:
  {
    if (lastState != currentState)
    {
      measureWaterStateEnter();
    }

    nextState = measureWaterState();
    break;
  }
  case CALIBRATION_FSM_FINISH:
  {
    nextState = finishState();
  }
  case CALIBRATION_FSM_ERROR:
  {
    break;
  }
  default:
  {
    assert(false);
  }
  }

  lastState = currentState;
  currentState = nextState;
}

CalibrationFsmState_t CalibrationFsm::idleState()
{
  if (calibrationRequested)
  {
    calibrationRequested = false;
    return CALIBRATION_FSM_START;
  }

  return CALIBRATION_FSM_IDLE;
}

void CalibrationFsm::startStateEnter()
{
  assert(currentState == CALIBRATION_FSM_START);

  this->plant->ledOn();
}

CalibrationFsmState_t CalibrationFsm::startState()
{
  assert(currentState == CALIBRATION_FSM_START);

  return CALIBRATION_FSM_START_DRY;
}

void CalibrationFsm::startDryStateEnter()
{
  assert(currentState == CALIBRATION_FSM_START_DRY);

  this->ledCalibrationDry->turnOn();
}

CalibrationFsmState_t CalibrationFsm::startDryState()
{
  assert(currentState == CALIBRATION_FSM_START_DRY);

  if (buttonSet->isPressed())
  {
    return CALIBRATION_FSM_MEASURE_DRY;
  }

  return CALIBRATION_FSM_START_DRY;
}

void CalibrationFsm::measureDryStateEnter()
{
  assert(currentState == CALIBRATION_FSM_MEASURE_DRY);

  this->ledCalibrationDry->blink(500);
  this->plant->startMeasurement();
}

CalibrationFsmState_t CalibrationFsm::measureDryState()
{
  assert(currentState == CALIBRATION_FSM_MEASURE_DRY);

  if (!this->plant->measure())
  {
    /* Measurement not finished. */
    return CALIBRATION_FSM_MEASURE_DRY;
  }

  /* Measurement finished. Recalibrate and continue. */
  this->plant->calibrateUpperHumidityLimit();
  this->ledCalibrationDry->turnOff();
  return CALIBRATION_FSM_START_WATER;
}

void CalibrationFsm::startWaterStateEnter()
{
  assert(currentState == CALIBRATION_FSM_START_WATER);

  this->ledCalibrationWater->turnOn();
}

CalibrationFsmState_t CalibrationFsm::startWaterState()
{
  assert(currentState == CALIBRATION_FSM_START_WATER);

  if (buttonSet->isPressed())
  {
    return CALIBRATION_FSM_MEASURE_WATER;
  }

  return CALIBRATION_FSM_START_WATER;
}

void CalibrationFsm::measureWaterStateEnter()
{
  assert(currentState == CALIBRATION_FSM_MEASURE_WATER);

  this->ledCalibrationWater->blink(500);
  this->plant->startMeasurement();
}

CalibrationFsmState_t CalibrationFsm::measureWaterState()
{
  assert(currentState == CALIBRATION_FSM_MEASURE_WATER)
  ;

  if (!this->plant->measure())
  {
    /* Measurement not finished. */
    return CALIBRATION_FSM_MEASURE_WATER;
  }

  /* Measurement finished. Recalibrate and continue. */
  this->plant->calibrateLowerHumidityLimit();
  this->ledCalibrationWater->turnOff();
  return CALIBRATION_FSM_FINISH;
}

CalibrationFsmState_t CalibrationFsm::finishState()
{
  assert(currentState == CALIBRATION_FSM_FINISH);

  calibrationData_t calibrationData;

  this->plant->getCurrentCalibration(&calibrationData);
  this->calibrationStorage->writeCalibrationData(this->plant->getID(), &calibrationData);

  this->plant->ledOff();

  return CALIBRATION_FSM_IDLE;
}

CalibrationFsmState_t CalibrationFsm::getCurrentState()
{
  return this->currentState;
}

void CalibrationFsm::startCalibration()
{
  assert(currentState == CALIBRATION_FSM_IDLE);

  this->calibrationRequested = true;
}
