#include "crc.h"
#include "calibration/calibration_data.h"

bool calibrationData_isValid(const calibrationData_t *calibrationData)
{
  uint16_t crc = calibrationData_calculateCrc(calibrationData);

  return crc == calibrationData->crc;
}

void calibrationData_updateCrc(calibrationData_t *calibrationData)
{
  calibrationData->crc = calibrationData_calculateCrc(calibrationData);
}

uint16_t calibrationData_calculateCrc(const calibrationData_t *calibrationData)
{
  return crc_ccitt((uint8_t *) calibrationData, sizeof(*calibrationData) - sizeof(uint16_t));
}

calibrationData_t calibrationData_getDefault(void)
{
  calibrationData_t calibrationData = {0};

  calibrationData.irrigationDuration_ms = 1000;
  calibrationData.maxValue = 2700;
  calibrationData.minValue = 950;

  return calibrationData;
}
