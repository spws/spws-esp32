#include "plant_fsm.h"
#include "calibration/calibration_storage.h"
#include "main_fsm.h"

void MainFsm::begin(
  Plant *plants,
  CalibrationStorage *calibrationStorage,
  Led *ledCalibrationWater,
  Led *ledCalibrationDry,
  Button *buttonStart,
  Button *buttonSet
)
{
  size_t i = 0;

  for (i = 0; i < CONFIG_PLANT_COUNT; i++)
  {
    plantFsms[i].begin(&plants[i]);
    calibrationFsms[i].begin(&plants[i], calibrationStorage, ledCalibrationWater, ledCalibrationDry, buttonStart, buttonSet);
  }

  this->ledCalibrationDry = ledCalibrationDry;
  this->ledCalibrationWater = ledCalibrationWater;

  this->buttonSet = buttonSet;
  this->buttonStart = buttonStart;

  currentState = MAIN_FSM_IDLE;
  lastState = MAIN_FSM_IDLE;

  /* Used to allow calibration only after startup. */
  isCalibrationAllowed = true;

  initialized = true;
}

void MainFsm::run()
{
  assert(initialized);

  MainFsmState_t nextState = currentState;

  if (currentState != lastState)
  {
    printf("new main fsm state: %d\n", currentState);
  }

  switch(currentState)
  {
  case MAIN_FSM_IDLE:
  {
    nextState = idleState();
    break;
  }
  case MAIN_FSM_RUNNING:
  {
    nextState = runningState();
    break;
  }
  case MAIN_FSM_CALIBRATION:
  {
    if (lastState != currentState)
    {
      calibrationStateEnter();
    }
    nextState = calibrationState();
    break;
  }
  case MAIN_FSM_ERROR:
  {
    /* TODO: Safe state + indicate error */
    break;
  }
  default:
  {
    assert(false);
  }
  }

  lastState = currentState;
  currentState = nextState;
}

MainFsmState_t MainFsm::idleState()
{
  assert(currentState == MAIN_FSM_IDLE);

  if (isCalibrationAllowed && buttonStart->isPressed())
  {
    isCalibrationAllowed = false;
    return MAIN_FSM_CALIBRATION;
  }

  if (isCalibrationAllowed && millis() > calibrationAllowedUntil_ms)
  {
    /* Time for starting the calibration after startup is over. */
    isCalibrationAllowed = false;
  }

  return MAIN_FSM_IDLE;
}

MainFsmState_t MainFsm::runningState()
{
  assert(currentState == MAIN_FSM_RUNNING);

  size_t i = 0;

  /* Run plant fsms. */
  for (i = 0; i < CONFIG_PLANT_COUNT; i++)
  {
    plantFsms[i].run();
  }

  /* Check if plant fsms are still running. */
  for (i = 0; i < CONFIG_PLANT_COUNT; i++)
  {
    if (plantFsms[i].getCurrentState() != PLANT_FSM_IDLE)
    {
      return MAIN_FSM_RUNNING;
    }
  }

  /* All plant fsm are idling. */
  return MAIN_FSM_IDLE;
}

void MainFsm::calibrationStateEnter()
{
  assert(currentState == MAIN_FSM_CALIBRATION);

  size_t i = 0;

  currentCalibrationFsm = 0;

  for (i = 0; i < CONFIG_PLANT_COUNT; i++)
  {
    calibrationFsms[i].startCalibration();
  }
}

MainFsmState_t MainFsm::calibrationState()
{
  assert(currentState == MAIN_FSM_CALIBRATION);

  calibrationFsms[currentCalibrationFsm].run();

  if (calibrationFsms[currentCalibrationFsm].getCurrentState() == CALIBRATION_FSM_IDLE)
  {
    /* Calibration for this plant finished. Run for next fsm. */
    currentCalibrationFsm++;
  }

  if (currentCalibrationFsm == CONFIG_PLANT_COUNT)
  {
    ESP.restart();

    /* Should not be reached. */
    assert(0);
    return MAIN_FSM_CALIBRATION;
  }

  return MAIN_FSM_CALIBRATION;
}

MainFsmState_t MainFsm::getCurrentState()
{
  assert(initialized);
  return currentState;
}
