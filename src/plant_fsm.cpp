#include "plant.h"
#include "plant_fsm.h"

void PlantFsm::begin(Plant *plant)
{
  this->plant = plant;
}

void PlantFsm::run()
{
  PlantFsmState_t nextState = currentState;

  switch(currentState)
  {
  case PLANT_FSM_IDLE:
  {

    break;
  }
  case PLANT_FSM_MEASURING:
  {
    if (currentState != lastState)
    {
      printf("Measuring state entered\n");
      measuringStateEnter();
    }

    nextState = measuringState();

    break;
  }
  case PLANT_FSM_IRRIGATING:
  {
    if (currentState != lastState)
    {
      printf("Irritating state entered\n");
      irrigatingStateEnter();
    }

    nextState = irrigatingState();

    break;
  }
  case PLANT_FSM_WAITING:
  {
    if (currentState != lastState)
    {
      printf("Waiting state entered\n");
      waitingStateEntered();
    }

    nextState = waitingState();
    break;
  }
  case PLANT_FSM_CALIBRATING:
  {
    if (currentState != lastState)
    {
      printf("Calibrating state entered\n");
      calibratingStateEntered();
    }

    nextState = calibrationState();
    break;
  }
  case PLANT_FSM_ERROR:
  {
    if (currentState != lastState)
    {
      printf("Error state entered\n");
    }
  }
  }

  lastState = currentState;
  currentState = nextState;
}

PlantFsmState_t PlantFsm::getCurrentState()
{
  return currentState;
}

void PlantFsm::measuringStateEnter()
{
  assert(currentState == PLANT_FSM_MEASURING);

  plant->startMeasurement();
}

PlantFsmState_t PlantFsm::measuringState()
{
  assert(currentState == PLANT_FSM_MEASURING);

  if (!plant->measure())
  {
    /* Measurement not finished. */
    return PLANT_FSM_MEASURING;
  }

  /* Measurement finished. */

  /* Recalibrate before checking if the plant requires humidity to consider
   * potential new sensor calibration. */
  plant->calibrateHumidityLimits();

  if (!plant->needsIrrigation())
  {
    return PLANT_FSM_IDLE;
  }

  return PLANT_FSM_IRRIGATING;
}

void PlantFsm::irrigatingStateEnter()
{
  assert(currentState == PLANT_FSM_IRRIGATING);

  plant->startIrrigation();
}

PlantFsmState_t PlantFsm::irrigatingState()
{
  assert(currentState == PLANT_FSM_IRRIGATING);

  if (plant->irrigate())
  {
    /* Irrigation not finished. */
    return PLANT_FSM_IRRIGATING;
  }

  return PLANT_FSM_WAITING;
}

void PlantFsm::waitingStateEntered()
{
  assert(currentState == PLANT_FSM_WAITING);

  waitingStartedAt_ms = millis();
}

PlantFsmState_t PlantFsm::waitingState()
{
  assert(currentState == PLANT_FSM_WAITING);

  uint32_t currentTime_ms = millis();

  if (currentTime_ms - waitingStartedAt_ms >= waitingTime_ms)
  {
    return PLANT_FSM_CALIBRATING;
  }

  return PLANT_FSM_WAITING;
}

void PlantFsm::calibratingStateEntered()
{
  assert(currentState = PLANT_FSM_CALIBRATING);

  plant->startMeasurement();
}

PlantFsmState_t PlantFsm::calibrationState()
{
  assert(currentState = PLANT_FSM_CALIBRATING);

  if (!plant->measure())
  {
    /* Measurement not finished. */
    return PLANT_FSM_CALIBRATING;
  }

  plant->calibrateIrrigationDuration();

  return PLANT_FSM_IDLE;
}
