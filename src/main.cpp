#include "Arduino.h"
#include "WiFi.h"

#include "config.h"
#include "mqtt.h"
#include "water_tank.h"
#include "pump.h"
#include "humidity_sensor.h"
#include "plant.h"
#include "led.h"
#include "button.h"
#include "hardware_test.h"
#include "calibration/calibration_storage.h"
#include "main_fsm.h"
#include "main.h"

WiFiClient wifi_client;

WaterTank water_tank = {CONFIG_WATER_TANK_SENSOR_PIN,
    CONFIG_WATER_TANK_EMPTY_WHEN_SENSOR_CLOSED};

Pump pumps[] = {
  {CONFIG_PUMP_0_POWER_PIN, &water_tank},
  {CONFIG_PUMP_1_POWER_PIN, &water_tank},
};
HumiditySensor humiditySensors[] = {
  {CONFIG_HUMIDITY_SENSOR_0_DATA_PIN, CONFIG_HUMIDITY_SENSOR_0_POWER_PIN},
  {CONFIG_HUMIDITY_SENSOR_1_DATA_PIN, CONFIG_HUMIDITY_SENSOR_1_POWER_PIN},
};

Led ledPlants[] = {
  CONFIG_LED_PLANT_0,
  CONFIG_LED_PLANT_1
};

Plant plants[] = {
  {0u, CONFIG_PLANT_0_NAME, &humiditySensors[0], &pumps[0], &ledPlants[0], 30},
  {1u, CONFIG_PLANT_1_NAME, &humiditySensors[1], &pumps[1], &ledPlants[1], 30},
};

Led ledCalibrationWater = CONFIG_LED_CALIBRATION_WET;
Led ledCalibrationDry = CONFIG_LED_CALIBRATION_DRY;

Button buttonStart = {CONFIG_BUTTON_START, HIGH, 10u};
Button buttonSet = {CONFIG_BUTTON_SET, HIGH, 10u};

CalibrationStorage calibrationStorage;

MainFsm mainFsm;

void setup()
{
  size_t i = 0;
  calibrationData_t calibrationData = {0};
  bool valid = false;

  /* Initialize serial connection. */
  Serial.begin(9600);
  while (! Serial);
  Serial.printf("Booting...\n");

#if ! CONFIG_HARDWARE_TEST && not defined DISABLE_MQTT
  /* Initialize WiFi connection. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(CONFIG_WIFI_SSID, CONFIG_WIFI_PASSWORD);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("WiFi connection failed! Rebooting...");
    ESP.restart();
  }

  /* Initialize MQTT connection. */
  Mqtt::init(CONFIG_MQTT_CLIENT_NAME, wifi_client, CONFIG_MQTT_SERVER_IP, CONFIG_MQTT_SERVER_PORT);

#endif

  buttonStart.begin();
  buttonSet.begin();

  ledCalibrationDry.begin();
  ledCalibrationWater.begin();

  calibrationStorage.begin();

  for (i = 0; i < CONFIG_PLANT_COUNT; i++)
  {
    ledPlants[i].begin();
  }

  for (i = 0; i < CONFIG_PLANT_COUNT; i++)
  {
    valid = calibrationStorage.getCalibrationData(i, &calibrationData);
    if (!valid)
    {
      printf("WARNING: Plant %u has no valid calibration data. Using default values...\n", i);
      calibrationData = calibrationData_getDefault();
    }

    printf("calibration data for plant %u:\n", i);
    printf("\tirrigation duration [ms]: %u\n", calibrationData.irrigationDuration_ms);
    printf("\tsensor minimum value: %u\n", calibrationData.minValue);
    printf("\tsensor maximum value: %u\n", calibrationData.maxValue);

    plants[i].begin(&calibrationData);
  }

  mainFsm.begin(plants, &calibrationStorage, &ledCalibrationWater, &ledCalibrationDry, &buttonStart, &buttonSet);
}

void deepSleepForMinutes(unsigned int minutes)
{
  uint64_t us = minutes * 60ULL * 1000ULL * 1000ULL;
  esp_sleep_enable_timer_wakeup(us);
  esp_deep_sleep_start();
}

void loop()
{
  size_t i = 0;
#if CONFIG_HARDWARE_TEST
  runHardwareTest(water_tank, humiditySensors, pumps, CONFIG_PLANT_COUNT);
#else

  mainFsm.run();

  for (i = 0; i < CONFIG_PLANT_COUNT; i++)
  {
    ledPlants[i].handle();
  }
  ledCalibrationDry.handle();
  ledCalibrationWater.handle();

  buttonStart.handle();
  buttonSet.handle();

  return;

  // Plant::irrigateMultiplePlantsIfNecessary(plants, CONFIG_PLANT_COUNT);

  /* Delay is necessary so that all MQTT messages are sent
   * before deep sleep.
   */
  delay(50);
  deepSleepForMinutes(30);
#endif
}
