#include "hardware_test.h"

static void waitForWaterTankChange(WaterTank &water_tank)
{
	bool is_empty, new_is_empty;
	is_empty = water_tank.isEmpty();
	Serial.println("Change water tank is empty value to continue...");
	do {
		delay(100);
		new_is_empty = water_tank.isEmpty();
	} while(is_empty == new_is_empty);
	delay(1000);
}

static void testWaterTank(WaterTank &water_tank)
{
	bool is_empty;

	is_empty = water_tank.isEmpty();
	Serial.printf("Water Tank is empty: %d\n", is_empty);

	waitForWaterTankChange(water_tank);

	is_empty = water_tank.isEmpty();
	Serial.printf("Water Tank is empty: %d\n", is_empty);
}

static void testHumiditySensorsPowerPin(HumiditySensor humiditySensors[], unsigned int sensor_count)
{
	unsigned int i;

	for (i = 0; i < sensor_count; i ++) {
		Serial.printf("Power on humidity sensor %d\n", i);
		humiditySensors[0].powerOn();
	}
}

static void testHumiditySensorsDataPin(HumiditySensor humiditySensors[], unsigned int sensor_count)
{
	unsigned int i;

	for (i = 0; i < sensor_count; i ++) {
		Serial.printf("Humidity sensor %d: %d\n", i, humiditySensors[i].readDataPin());
	}
}

static void powerOffHumiditySensors(HumiditySensor humiditySensors[], unsigned int sensor_count)
{
	unsigned int i;

	for (i = 0; i < sensor_count; i ++) {
		humiditySensors[i].powerOff();
	}
}

static void testHumiditySensors(HumiditySensor humiditySensors[], unsigned int sensor_count, WaterTank &water_tank)
{
	testHumiditySensorsPowerPin(humiditySensors, sensor_count);
	waitForWaterTankChange(water_tank);
	testHumiditySensorsDataPin(humiditySensors, sensor_count);
	powerOffHumiditySensors(humiditySensors, sensor_count);

	Serial.println("Test finished");
}

static void testPumps(Pump pumps[], unsigned int pump_count, WaterTank &water_tank)
{
	unsigned int i;

	for (i = 0; i < pump_count; i++) {
		Serial.printf("Power up pump: %d\n", i);
		pumps[i].activate();
		waitForWaterTankChange(water_tank);
		pumps[i].deactivate();
	}
}

void runHardwareTest(
	WaterTank &water_tank,
	HumiditySensor humiditySensors[],
	Pump pumps[],
	unsigned int plant_count
)
{
	testWaterTank(water_tank);
	testHumiditySensors(humiditySensors, plant_count, water_tank);
	testPumps(pumps, plant_count, water_tank);
}
