#include "mqtt.h"

PubSubClient *Mqtt::mqtt_client = nullptr;
const char *Mqtt::client_name = nullptr;

void Mqtt::init(const char *client_name, Client& network_client, const char *server_ip, uint16_t server_port)
{
	Mqtt::client_name = client_name;
	Mqtt::mqtt_client = new PubSubClient(network_client);
	Mqtt::mqtt_client->setServer(server_ip, server_port);

	Serial.printf("MQTT initalized\n");
}

bool Mqtt::publish(const char *topic, const char *payload)
{
	PubSubClient *client = Mqtt::mqtt_client;
	if (! client) {
		return false;
	}

	if (! client->connected() && ! Mqtt::connect()) {
		return false;
	}

	Serial.printf("MQTT: [%s]: %s\n", topic, payload);

	return client->publish(topic, payload);
}

bool Mqtt::publish_humidity(const char *plant_name, int humidity)
{
	/* Generate MQTT topic.
	 * + for '/' and '\0'
	 */
	char topic[strlen(CONFIG_MQTT_HUMIDITY_TOPIC) + strlen(plant_name) + 2];
	sprintf(topic, CONFIG_MQTT_HUMIDITY_TOPIC "/%s", plant_name);

	/* Convert humidity to string. */
	char payload[10];
	sprintf(payload, "%d", humidity);

	return Mqtt::publish(topic, payload);
}

bool Mqtt::connect()
{
	PubSubClient *client = Mqtt::mqtt_client;
	if (! client) {
		return false;
	}

	while (! client->connected()) {
		if (client->connect(Mqtt::client_name)) {
			Serial.printf("MQTT client connected\n");
		} else {
			Serial.printf("Error connecting MQTT client: %d", client->state());
			delay(1000);
		}
	}

	return true;
}
