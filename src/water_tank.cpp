#include "water_tank.h"

WaterTank::WaterTank(uint8_t sensorPin, bool empty_when_sensor_closed)
{
	this->sensorPin = sensorPin;
	this->empty_when_sensor_closed = empty_when_sensor_closed;

	/* INPUT_PULLUP: LOW when pressed, HIGH when released. */
	pinMode(sensorPin, INPUT_PULLUP);
}

bool WaterTank::isEmpty()
{
	if (empty_when_sensor_closed) {
		return ! digitalRead(this->sensorPin);
	}
	return digitalRead(this->sensorPin);
}
