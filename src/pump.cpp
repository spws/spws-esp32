#include "pump.h"

Pump::Pump(uint8_t powerPin, WaterTank *water_tank)
{
	this->powerPin = powerPin;
	this->water_tank = water_tank;

	pinMode(powerPin, OUTPUT);
	digitalWrite(powerPin, LOW);
}

void Pump::activate(void)
{
	digitalWrite(this->powerPin, HIGH);
}

void Pump::deactivate(void)
{
	digitalWrite(this->powerPin, LOW);
}

void Pump::pumpForSeconds(unsigned int duration)
{
	unsigned int i;

	if (this->water_tank->isEmpty()) {
		return;
	}

	this->activate();

	for (i = 0; i < duration; i++) {
		if (this->water_tank->isEmpty()) {
			break;
		}

		delay(1000);
	}

	this->deactivate();
}
