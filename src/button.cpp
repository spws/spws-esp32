#include "button.h"

Button::Button(uint8_t pin, uint8_t activeAt, uint32_t debounceTime_ms)
{
  this->pin = pin;
  this->activeAt = activeAt;
  this->debounceTime_ms = debounceTime_ms;
}

void Button::begin()
{
  switch (this->activeAt)
  {
  case HIGH:
  {
    pinMode(this->pin, INPUT_PULLDOWN);
    break;
  }
  case LOW:
  {
    pinMode(this->pin, INPUT_PULLUP);
    break;
  }
  default:
  {
    assert(false);
    break;
  }
  }

  currentState = digitalRead(this->pin);
  debounceTimeStartetAt_ms = 0;
}

bool Button::isPressed()
{
  return currentState == this->activeAt;
}

void Button::handle()
{
  int buttonState = digitalRead(this->pin);
  uint32_t currentTime_ms = millis();

  if (buttonState == currentState)
  {
    /* State didn't change. */
    return;
  }

  if (debounceTimeStartetAt_ms == 0)
  {
    /* State changed. Restart debouncing. */
    debounceTimeStartetAt_ms = millis();
    return;
  }

  if (currentTime_ms - debounceTime_ms < debounceTimeStartetAt_ms)
  {
    /* Debounce not finished. */
    return;
  }

  /* Debounce finished. Set new state. */
  currentState = buttonState;
  debounceTimeStartetAt_ms = 0;
}
