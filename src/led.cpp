#include "led.h"

Led::Led(uint8_t pin)
{
  this->pin = pin;
}

void Led::begin()
{
  pinMode(this->pin, OUTPUT);
  digitalWrite(this->pin, LOW);

  this->currentState = LOW;
  this->blinkInterval_ms = 0;
}

void Led::blink(uint32_t interval_ms)
{
  this->blinkInterval_ms = interval_ms;
  this->blinkChangedAt_ms = millis();
  this->changeStatus();
}

void Led::turnOn()
{
  this->currentState = HIGH;
  digitalWrite(this->pin, HIGH);

  this->blinkInterval_ms = 0;
  this->blinkChangedAt_ms = 0;
}

void Led::turnOff()
{
  this->currentState = LOW;
  digitalWrite(this->pin, LOW);

  this->blinkInterval_ms = 0;
  this->blinkChangedAt_ms = 0;
}

void Led::handle()
{
  if (blinkInterval_ms == 0)
  {
    /* Blinking not configured. */
    return;
  }

  if (millis() - this->blinkChangedAt_ms < this->blinkInterval_ms)
  {
    /* Interval not passed. */
    return;
  }

  this->changeStatus();
  this->blinkChangedAt_ms += this->blinkInterval_ms;
}

void Led::changeStatus()
{
  if (this->currentState == HIGH)
  {
    this->currentState = LOW;
    digitalWrite(this->pin, LOW);
  }
  else
  {
    this->currentState = HIGH;
    digitalWrite(this->pin, HIGH);
  }
}
