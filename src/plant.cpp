#include "plant.h"

Plant::Plant(uint32_t id, const char *name, HumiditySensor *humiditySensor, Pump *pump, Led *led,int humidityLowThreshold)
{
  this->id = id;
  this->name = name;
  this->humiditySensor = humiditySensor;
  this->pump = pump;
  this->humidityLowThreshold = humidityLowThreshold;
  this->led = led;
}

void Plant::begin(const calibrationData_t *calibrationData)
{
  this->irrigationDuration_ms = calibrationData->irrigationDuration_ms;

  this->humiditySensor->begin(calibrationData);

  /* Shoud be the defaut value, but just to be sure... */
  pump->deactivate();
}

void Plant::startMeasurement()
{
  humiditySensor->startMeasurement();
}

bool Plant::measure()
{
  return humiditySensor->measure();
}

bool Plant::needsIrrigation()
{
  int8_t humidity = 0;

  humidity = humiditySensor->getMeasuredHumidity();

  if (humidity < 0) {
    /* Error */
    return false;
  }

  if (humidity < humidityLowThreshold)
  {
    /* Plant is too dry. */
    return true;
  }
  else
  {
    /* Plant is still humid enough. */
    return false;
  }
}

void Plant::startIrrigation()
{
  pump->activate();
  irrigationStartedAt_ms = millis();
}

bool Plant::irrigate(void)
{
  if (millis() - irrigationStartedAt_ms > irrigationDuration_ms)
  {
    pump->deactivate();
    return false;
  }

  return true;
}

void Plant::calibrateHumidityLimits()
{
  humiditySensor->calibrateHumidityLimits();
}

void Plant::calibrateUpperHumidityLimit()
{
  humiditySensor->calibrateUpperHumidityLimit();
}

void Plant::calibrateLowerHumidityLimit()
{
  humiditySensor->calibrateLowerHumidityLimit();
}

void Plant::calibrateIrrigationDuration()
{
  int8_t humidity = -1;
  int8_t difference = 0;

  humidity = humiditySensor->getMeasuredHumidity();
  if (humidity == -1)
  {
    /* Cannot calibrate with invalid humidity value. */
    return;
  }

  assert(humidity >= 0);
  assert(humidity <= 100);

  difference = humidity - targetHumidity;

  if (difference <= targetTollerance || difference >= -targetTollerance)
  {
    /* Perfect irrigation duration. */
    return;
  }

  /* difference < 0 => irrigation was too short.
   * difference > 0 => irrigation was too long. */
  this->irrigationDuration_ms -= difference * 25; /* 25 ms for each percentage difference. */
}

void Plant::getCurrentCalibration(calibrationData_t *calibrationData)
{
  calibrationData->irrigationDuration_ms = this->irrigationDuration_ms;
  calibrationData->minValue = this->humiditySensor->getMinValue();
  calibrationData->maxValue = this->humiditySensor->getMaxValue();
}

uint32_t Plant::getID()
{
  return this->id;
}

void Plant::ledBlink(uint32_t interval_ms)
{
  this->led->blink(interval_ms);
}

void Plant::ledOff()
{
  this->led->turnOff();
}

void Plant::ledOn()
{
  this->led->turnOn();
}
